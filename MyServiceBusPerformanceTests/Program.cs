﻿using System;
using System.Threading.Tasks;
using MyDependencies;
using MyServiceBus.Domains;
using MyServiceBus.Domains.Execution;
using MyServiceBus.Domains.MessagesContent;
using MyServiceBus.Domains.Sessions;
using MyServiceBus.Domains.Topics;

namespace MyServiceBusPerformanceTests
{
    class Program
    {
        static void Main(string[] args)
        {
            var amount = TestPublishersWithNoSubscribers(TimeSpan.FromSeconds(1)).Result;
            
            Console.WriteLine("Publishes with No Subscribers in second: "+amount);
        }

        private static async Task<int> TestPublishersWithNoSubscribers(TimeSpan testDuration)
        {
            var ioc = new MyIoc();

            ioc.RegisterMyNoServiceBusDomainServices();
            ioc.BindPerformanceTestServices();

            var myServiceBusPublisher = ioc.GetService<MyServiceBusPublisher>();
            
            var subscriber = ioc.GetService<MyServiceBusSubscriber>();


            var sessionsList = ioc.GetService<SessionsList>();


            var session = sessionsList.NewSession("MyName", "127.0.0.1", DateTime.UtcNow, TimeSpan.FromMinutes(1), 0);


            var topicsManager = ioc.GetService<TopicsManagement>();

            var topic = await topicsManager.AddIfNotExistsAsync("test-topic");
            var message = new byte[] {1, 2, 3, 4, 5};
            var startTest = DateTime.UtcNow;
            var nowTime = DateTime.UtcNow;

            var result = 0;
            
            //subscriber.SubscribeToQueueAsync()
            
    
            
            while (nowTime-startTest < testDuration)
            {
                await myServiceBusPublisher.PublishAsync(session, topic.TopicId, new[] {message}, DateTime.UtcNow, false);
                nowTime = DateTime.UtcNow;
                result++;
            }

            return result;

        } 
        
        
    }
}