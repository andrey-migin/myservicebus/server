using System;
using MyDependencies;
using MyServiceBus.Domains;
using MyServiceBus.Domains.MessagesContent;
using MyServiceBus.Domains.Persistence;
using MyServiceBus.Persistence.AzureStorage;
using MyServiceBus.Persistence.AzureStorage.PageBlob;
using MyServiceBus.Persistence.AzureStorage.TopicMessages;

namespace MyServiceBusPerformanceTests
{
    public static class PerformanceTestServices
    {

        public static void BindPerformanceTestServices(this IServiceRegistrator sr)
        {
            sr.Register<IMessagesPersistentStorage>(new MessagesPersistentStorage(
                (topic,pageId)=>new PageBlobInMem()));

            sr.Register<IMessagesToPersistQueue>(new MessagesToPersistQueue());

            sr.Register<IMyServiceBusSettings>(new PerformanceServiceBusSettings());
            
            sr.Register<ITopicPersistenceStorage>(new TopicsPersistentStorageAzureBlobs(new PageBlobInMem()));
        }

    }

    public class PerformanceServiceBusSettings : IMyServiceBusSettings
    {
        private static readonly TimeSpan _eventuallyPersistenceDelay = TimeSpan.FromSeconds(5);
        public TimeSpan EventuallyPersistenceDelay => _eventuallyPersistenceDelay;
    }

}