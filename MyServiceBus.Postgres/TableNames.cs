namespace MyServiceBus.Postgres
{
    public static class TableNames
    {

        public static string Queues = "queues";
        public static string Consumers = "consumers";
        public static string Messages = "messages";

    }
}