using MyServiceBus.Domains.Topics;

namespace MyServiceBus.Postgres.Dto
{
    public class QueueMessageReferencePostgresEntity
    {
        public string TopicId { get; set; }
        public string QueueId { get; set; }
        public long MessageId { get; set; }
    }

}