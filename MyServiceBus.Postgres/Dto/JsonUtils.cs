using System;
using System.Text;

namespace MyServiceBus.Postgres.Dto
{
    public static class JsonUtils
    {
        public static string ToUtf8Json(this object src)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(src);
        }
        
        public static T ParseUtf8Json<T>(this string src)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(src);
        }
        
        public static T ParseUtf8Json<T>(this string src, Func<T> getDefault) where T:class
        {
            T result;
            try
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(src);
            }
            catch (Exception)
            {
                result = getDefault();
            }

            return result ?? getDefault();
        }


        public static string ToUtf8Base64(this string src)
        {
            var bytes = Encoding.UTF8.GetBytes(src);
            return Convert.ToBase64String(bytes);


        }
        
    }
}