using System;
using System.Collections.Generic;
using System.Linq;
using MyServiceBus.Domains.Persistence;
using MyServiceBus.Domains.Topics;

namespace MyServiceBus.Postgres.Dto
{

    public class MessageDto : IMyMessage
    {

        public long MessageId { get; set; }
        
        public string Data { get; set; }

        bool IMyMessage.Persisted
        {
            get => true;
            set {}
        }
        
        public DateTime Created { get; set; }

        byte[] IMyMessage.Data => GetData();
        
        public string TopicId { get; set; }

        private byte[] _data;

        internal byte[] GetData()
        {

            if (_data != null)
                return _data;

            if (Data == null)
                return _data = Array.Empty<byte>();

            try
            {
                return _data = Convert.FromBase64String(Data);
            }
            catch (Exception)
            {
                return _data = Array.Empty<byte>();
            }
        }


        public static MessageDto Create(string topicId, IMyMessage myMessage)
        {
            return new MessageDto
            {
                TopicId = topicId,
                MessageId = myMessage.MessageId,
                Data = Convert.ToBase64String(myMessage.Data)
            };
        } 
        
    }


    public class QueueIndexRangeDto : IQueueIndexRange
    {
        public long FromId { get; set; }
        public long ToId { get; set; }

        public static QueueIndexRangeDto Create(IQueueIndexRange src)
        {
            return new QueueIndexRangeDto
            {
                FromId = src.FromId,
                ToId = src.ToId
            };
        }
    }

    public class QueueIndexDto : IQueueSnapshot
    {
        public string QueueId { get; set; }

        IEnumerable<IQueueIndexRange> IQueueSnapshot.Ranges => Ranges;
         
         public QueueIndexRangeDto[] Ranges { get; set; }


         public static QueueIndexDto Create(string queueId, IEnumerable<IQueueIndexRange> src)
         {
             return new QueueIndexDto
             {
                 QueueId = queueId,
                 Ranges = src.Select(QueueIndexRangeDto.Create).ToArray()
             };
         }
    }

    public class QueueSnapshotDto : IQueueSnapshot
    {
        
        public string TopicId { get; set; }
        public string QueueId { get; set; }

        IEnumerable<IQueueIndexRange> IQueueSnapshot.Ranges => Intervals.ParseUtf8Json(Array.Empty<QueueIndexRangeDto>);

        public string Intervals { get; set; }

        public static QueueSnapshotDto Create(IQueueSnapshot src, string topicId)
        {
            return new QueueSnapshotDto
            {
                TopicId = topicId,
                QueueId = src.QueueId,
                Intervals = src.Ranges.ToDtoJson()
            };
        }
    }


    public static class MessageDtoResultHelpers
    {

        public static string ToDtoJson(this IEnumerable<IMyMessage> messages, string topicId)
        {
            var result = messages.Select(message => MessageDto.Create(topicId, message));
            return Newtonsoft.Json.JsonConvert.SerializeObject(result);
        }
        
        
        public static string ToDtoJson(this IEnumerable<IQueueSnapshot> messages, string topicId)
        {
            var result = messages.Select(itm => QueueSnapshotDto.Create(itm, topicId));
            return Newtonsoft.Json.JsonConvert.SerializeObject(result);
        }


        public static string ToDtoJson(this IEnumerable<IQueueIndexRange> src)
        {
            var objects = src.Select(QueueIndexRangeDto.Create);
            return Newtonsoft.Json.JsonConvert.SerializeObject(objects);
        }
        
    }


}