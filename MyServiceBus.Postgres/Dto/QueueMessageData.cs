using MyServiceBus.Domains;
using MyServiceBus.Domains.Topics;

namespace MyServiceBus.Postgres.Dto
{
    public class QueueMessageDataPostgresEntity
    {
        public long MessageId { get; set; }
        public string Data { get; set; }
    }
    
    public static class QueueMessageDataPostgresEntityHelpers{

        public static QueueMessageDataPostgresEntity ToQueueMessageDataPostgresEntity(this IMyMessage message)
        {
            return new QueueMessageDataPostgresEntity
            {
                Data = message.Data.ToBase64(),
                MessageId = message.MessageId
            };
        }
    }
}