﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetCoreDecorators;
using MyPostgreSQL;
using MyServiceBus.Domains.Persistence;
using MyServiceBus.Domains.Topics;
using MyServiceBus.Postgres.Dto;

namespace MyServiceBus.Postgres
{
    
    /*
    public class MessagesPersistentStorage : IMessagesPersistentStorage
    {
        private readonly IPostgresConnection _postgresConnection;
        private readonly string _schema;

        public MessagesPersistentStorage(IPostgresConnection postgresConnection, string schema)
        {
            _postgresConnection = postgresConnection;
            _schema = schema;
        }

        public async Task SaveAsync(string topicId, IEnumerable<IMyMessage> messages, 
            IEnumerable<IQueueSnapshot> queueSnapshots, long minMessageId)
        {
            var modelMessages = messages.ToDtoJson(topicId).ToUtf8Base64();

            var modelIndices = queueSnapshots.ToDtoJson(topicId).ToUtf8Base64();

            try
            {
                var sql = $"SELECT * FROM {_schema}.enqueue_messages(@topicId, '{modelMessages}', '{modelIndices}');";
                
                Console.WriteLine(sql);
                await _postgresConnection.ExecAsync(sql, new {topicId});
            }
            catch (Exception e)
            {
                Console.WriteLine("Messages Model:" + modelMessages);
                Console.WriteLine("Indices Model:" + modelIndices);
                Console.WriteLine(e);
                throw;
            }
        }

        public ValueTask<long> GetMaxMessageId(string topicId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<(string topicId, IReadOnlyList<IQueueSnapshot> snapshots)>> GetQueueIndexStatesAsync()
        {
            var sql = $"SELECT * FROM {_schema}.{TableNames.Queues};";

            var resultRecords = await _postgresConnection.GetRecordsAsync<QueueSnapshotDto>(sql);


            var result = new List<(string topicId, IReadOnlyList<IQueueSnapshot> indices)>();
            
            foreach (var group in resultRecords.GroupBy(itm => itm.TopicId))
            {
                result.Add((group.Key, group.Cast<IQueueSnapshot>().AsReadOnlyList()));
            }

            return result;
        }

        public async Task<IReadOnlyList<IMyMessage>> GetMessagesAsync(string topicId, long fromId, int limit)
        {
            var sql = $"SELECT * FROM {_schema}.{TableNames.Messages} WHERE topicid=@topicId AND messageid>={fromId} LIMIT {limit}";

            return (await _postgresConnection.GetRecordsAsync<MessageDto>(sql, new {topicId}))
                .AsReadOnlyList();
        }

    }
    */
}