
/*
  DROP FUNCTION enqueue_messages(text)
 */

CREATE OR REPLACE FUNCTION init()
    RETURNS int
AS $$


BEGIN

  UPDATE
    myservicebus.queues
  SET 
    leased = null
  WHERE 
    leased is not null;

  return 0;

END; $$

LANGUAGE 'plpgsql';