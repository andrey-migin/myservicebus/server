
/*
  DROP FUNCTION get_messages(text, text, int, text)
 */

CREATE OR REPLACE FUNCTION get_messages(p_topic_id text, p_queue_id text, from_id bigint, p_limit int)
    RETURNS TABLE(
    messageid bigint,
    "data" text   
    )
    
AS $$

BEGIN
    
  RETURN QUERY 
    SELECT 
      q.messageid, m.data
    FROM 
      myservicebus.queues q
    JOIN 
      myservicebus.messages m 
    ON 
      m.topicid = p_topic_id AND m.messageid = q.messageid         
    WHERE
      q.topicid = p_topic_id AND
      q.queueid = p_queue_id AND
      q.messageid > from_id
    ORDER BY 
      q.messageid
     LIMIT p_limit;

END; $$

LANGUAGE 'plpgsql';