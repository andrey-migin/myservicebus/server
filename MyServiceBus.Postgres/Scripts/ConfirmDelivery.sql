
/*
  DROP FUNCTION confirm_delivery(text)
 */

CREATE OR REPLACE FUNCTION confirm_delivery(p_model_base64 text)
RETURNS json
AS $$

DECLARE
  p_remains int;
  json_model json;
    p_queue_id text;
    p_topic_id text;
    p_message_id bigint;
BEGIN

    json_model =  CONVERT_FROM(DECODE(p_model_base64, 'BASE64'), 'UTF-8')::json;
    

    FOR  p_topic_id, p_queue_id, p_message_id IN

        select * from json_to_recordset(json_model)
                          as x("TopicId" text,
                               "QueueId" text,
                               "MessageId" bigint)
        LOOP

            DELETE FROM
                myservicebus.queues q
            WHERE
                    q.topicid=p_topic_id
              AND
                    q.queueid=p_queue_id
              AND
                    q.messageid=p_message_id;
        
        end loop;



    FOR p_topic_id, p_message_id IN
    select "TopicId", "MessageId"
    from json_to_recordset(json_model)
                      as x("TopicId" text,
                           "QueueId" text,
                           "MessageId" bigint)
    GROUP BY "TopicId", "MessageId"
        LOOP
            SELECT count(*) INTO p_remains
            from myservicebus.queues
            WHERE
                    topicid=p_topic_id
              AND
                    messageid=p_message_id;

            IF (p_remains = 0) THEN
                DELETE FROM myservicebus.messages
                WHERE topicid=p_topic_id AND messageid=p_message_id;
            END IF;
        end loop;
    
    
    return json_model;


END; $$

LANGUAGE 'plpgsql';