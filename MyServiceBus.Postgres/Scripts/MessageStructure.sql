
/*
  DROP FUNCTION message_structure(text, bigint)
 */

CREATE OR REPLACE FUNCTION message_structure(p_topic_id text, p_messageid bigint)
    RETURNS json
AS $$

DECLARE
    p_result json;
    p_data text;
BEGIN

    SELECT array_to_json(array_agg(row))
    INTO p_result
    FROM
        (SELECT q.queueid as "id" FROM myservicebus.queues q where messageid = p_messageId) row;


    SELECT m.data
    INTO p_data
    FROM myservicebus.messages m
    WHERE m.topicid=p_topic_id AND m.messageid=p_messageId;



    SELECT
        row_to_json(row)
    INTO p_result
    FROM
        (SELECT
             p_data as "Data",
             p_messageId as "Messageid",
             p_result as "Queues"

         FROM myservicebus.queues q
         where messageid = p_messageId) row;


    return p_result;

END; $$

LANGUAGE 'plpgsql';