
/*
  DROP FUNCTION release_message(text, text)
 */

CREATE OR REPLACE FUNCTION release_message(p_topic_id text, p_queue_id text, p_messageid bigint)
    RETURNS json
AS $$


BEGIN

    UPDATE
        myservicebus.queues q
    SET
        leased = null
    WHERE
            q.topicid=p_topic_id
      AND
            q.queueid = p_queue_id
      AND q.messageid = p_messageId;

    return myservicebus.message_structure(p_topic_id, p_messageId);

END; $$

LANGUAGE 'plpgsql';