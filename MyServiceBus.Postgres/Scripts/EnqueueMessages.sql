
/*
  DROP FUNCTION enqueue_messages(text,text,text,text)
 */

CREATE OR REPLACE FUNCTION enqueue_messages(p_topicId text, base64_messages text, base64_indexes text, json_delete text)
    RETURNS json
AS $$

DECLARE
    json_model json;
    p_queueid text;
    p_intervals json;
    
BEGIN

    json_model =  CONVERT_FROM(DECODE(base64_messages, 'BASE64'), 'UTF-8')::json;

    
    INSERT INTO myservicebus.messages
    ("topicid", "messageid", "data")
     select * from json_to_recordset(json_model)
                          as x("TopicId" text,
                               "MessageId" bigint,
                               "Data" text
                              )
    ON CONFLICT
        ON CONSTRAINT messages_pk
        DO NOTHING;



    json_model =  CONVERT_FROM(DECODE(base64_indexes, 'BASE64'), 'UTF-8')::json;


    FOR p_queueid, p_intervals IN
        select * from json_to_recordset(json_model)
                 as x("QueueId" text,
                      "Intervals" json)
    LOOP    
      INSERT INTO myservicebus.queues
       ("topicid", "queueid", "intervals")
       VALUES 
           (p_topicId, p_queueid, p_intervals)
      ON CONFLICT
          ON CONSTRAINT queues_pk 
          DO UPDATE SET "intervals"=p_intervals;
    END LOOP;

    json_model =  CONVERT_FROM(DECODE(json_delete, 'BASE64'), 'UTF-8')::json;
    
    DELETE FROM myservicebus.messages
        WHERE topicid=p_topicId 
          AND messageid in 
              (select * from json_to_recordset(json_model) as x("Id" bigint));
    
    
    

return json_model;


END; $$

LANGUAGE 'plpgsql';