
/*
  DROP FUNCTION enqueue_messages_synch_cache(text, text, int, text)
 */

CREATE OR REPLACE FUNCTION enqueue_messages_synch_cache(p_topic_toget text, p_queue_toget text, p_limit int, p_model_base64 text)
    RETURNS json
AS $$

DECLARE
    json_model json;
    p_topicid text;
    p_messageid bigint;
    p_data text;
    p_json json;
BEGIN

    json_model =  CONVERT_FROM(DECODE(p_model_base64, 'BASE64'), 'UTF-8')::json;


    FOR p_topicid, p_messageid, p_data, p_json IN
        select * from json_to_recordset(json_model)
                          as x("TopicId" text,
                               "MessageId" bigint,
                               "Data" text,
                               "Queues" json
                )
        LOOP
            INSERT INTO myservicebus.messages
            ("messageid", "data", "topicid")
            VALUES (p_messageid, p_data, p_topicid)
            ON CONFLICT
                ON CONSTRAINT messages_pk
                DO NOTHING;

            INSERT INTO myservicebus.queues
            (topicid, queueid, messageid)

            select p_topicid, "Id", p_messageid
            from json_to_recordset(p_json) as x("Id" text)
            ON CONFLICT
                ON CONSTRAINT queues_pk
                DO NOTHING;
        END LOOP;



    SELECT
        array_to_json(array_agg(row))
    INTO p_json
    FROM
        (SELECT
             q.messageid as "Id"
         FROM
             myservicebus.queues q
         WHERE
                 q.topicid = p_topic_toget AND
                 q.queueid = p_queue_toget AND
             q.leased IS NULL
         ORDER BY q.messageid
         LIMIT p_limit) row;




    FOR p_messageid IN
        select * from json_to_recordset(p_json) as x("Id" text)
        LOOP
            UPDATE
                myservicebus.queues q
            SET
                leased = now() at time zone 'utc'
            WHERE
                    q.topicid = p_topic_toget AND
                    q.queueid = p_queue_toget AND
                    q.messageid = p_messageid;

        end loop;

    RETURN  p_json;


END; $$

LANGUAGE 'plpgsql';