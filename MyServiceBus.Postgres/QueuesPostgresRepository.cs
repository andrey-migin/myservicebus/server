using System.Collections.Generic;
using System.Threading.Tasks;
using MyPostgreSQL;
using MyServiceBus.Domains.Persistence;
using MyServiceBus.Domains.Queues;

namespace MyServiceBus.Postgres
{
    public class QueuePostgresEntity : IQueue
    {
        public string TopicId { get; set; }
        public string QueueId { get; set; }
        public bool DeleteOnDisconnect { get; set; }
        public static QueuePostgresEntity Create(IQueue src)
        {
            return new QueuePostgresEntity
            {
                TopicId = src.TopicId,
                QueueId = src.QueueId,
                DeleteOnDisconnect = src.DeleteOnDisconnect
            };
        }
    }
    
    
    public class QueuesPostgresRepository : IQueuesRepository
    {
        private readonly IPostgresConnection _postgresConnection;
        private readonly string _schema;

        public QueuesPostgresRepository(IPostgresConnection postgresConnection, string schema)
        {
            _postgresConnection = postgresConnection;
            _schema = schema;
        }

        public async Task AddAsync(IQueue queue)
        {
            var entity = QueuePostgresEntity.Create(queue);

            await _postgresConnection
                .Insert(_schema+'.'+TableNames.Consumers)
                .SetInsertModel(entity)
                .ExecuteAsync();
        }

        public async Task<IEnumerable<IQueue>> GetAsync()
        {
            var sql = $"SELECT * FROM {_schema+'.'+TableNames.Consumers}";

            return await _postgresConnection
                .GetRecordsAsync<QueuePostgresEntity>(sql);
        }

    }
}